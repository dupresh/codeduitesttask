﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodedUITestProject7.TestData
{
    public class TestPropData
    {
        /// Word
        //path to microsoft word 14 version
        public const string PATH_OF_WORD = @"C:\Program Files\Microsoft Office\Office14\WINWORD.EXE";
        //name of microsoft word procces
        public const string NAME_OF_WORD_PROCESS = "WINWORD";
        
        /// Excel
        public const string PATH_OF_EXCEL = @"C:\Program Files\Microsoft Office\Office14\EXCEL.EXE";
        //name of microsoft word procces
        public const string NAME_OF_EXCEL_PROCESS = "EXCEL";

        //ribbon button
        public const string INSERT = "Insert";
        public const string HOME = "Home";
        public const string FILE = "File";
        
        //name of file
        public const string NAME_OF_FILE = "testFile";
        
        //text for test 
        public const string TEXT_ONE = "Text 1 for Test Document";
        public const string TEXT_TWO = "Additional Text 2 for Text Document";

        //controlCommand
        public const string CONTROL_R = "\r";
    }
}

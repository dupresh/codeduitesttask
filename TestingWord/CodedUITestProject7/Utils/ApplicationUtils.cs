﻿using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodedUITestProject7.Utils
{
    public class ApplicationUtils
    {
        //start of application by path
        public void StartApplication(string path)
        {
            Process startApp = new Process();
            startApp = Process.Start(path);
        }

        //kill all application process using name of process
        public void KillAllAppProcess(string nameOfProcess)
        {
            foreach (Process proc in Process.GetProcessesByName(nameOfProcess))
            {
                try
                {
                    proc.Kill();
                }
                catch (Win32Exception winException)
                {
                    MessageBox.Show(winException.Message);
                }
                catch (InvalidOperationException invalidException)
                {
                    MessageBox.Show(invalidException.Message);
                }
            }
        }

        //get opened process by name
        public void GetProcessByName(string nameOfProcess)
        {
            Process.GetProcessesByName(nameOfProcess);
        }

        //show the desktop 
        public void ShowTheDesktop()
        {
            Shell32.Shell objShel = new Shell32.Shell();

            ((Shell32.IShellDispatch4)objShel).ToggleDesktop();
        }

        //close office client by pressing Ctr+F4
        public void CloseClient()
        {
            Keyboard.SendKeys("^{F4}");
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.Office.Interop.Word;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Text;
using System.Collections;
using CodedUITestProject7.TestData;



namespace CodedUITestProject7.WordApi
{
    public class WorkWithWord
    {
        //Adding text in the end 
        public void AddText(string text)
        {
            Application wordApp = (Application)Marshal.GetActiveObject("Word.Application");
            Thread.Sleep(2000);
            wordApp.Visible = true;
            Keyboard.SendKeys("{END}");             
            wordApp.Selection.TypeText(text);
            wordApp.Selection.TypeParagraph();
        }

        //Return full text
        public string GetAllText()
        {
            Application wordApp = (Application)Marshal.GetActiveObject("Word.Application");
            Thread.Sleep(2000);
            wordApp.Visible = true;
            Document wordDoc = wordApp.Documents.Application.ActiveDocument;
            Thread.Sleep(2000);
            wordDoc.ActiveWindow.SetFocus();
            Range wordRange = wordDoc.Range(0, Type.Missing);
            string text = "";
            for (int i = 1; i < wordDoc.Paragraphs.Count; i++)
            {
                string parText = wordDoc.Paragraphs[i].Range.Text;
                text += parText;
            }
            return text;
        }
    }
}

﻿namespace CodedUITestProject7.WordUIMaps.RibbonPanelUIMapClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Windows.Forms;
    using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;


    public partial class RibbonPanelUIMap
    {
        public const int WAIT_TIME = 2000;

        public WinTabPage uIRibbonTab { get { return this.UIDocument1MicrosoftWoWindow.UIItemWindow.UIRibbonClient.UIInsertTabPage; } }
        public WinButton uIHyperlinkButton { get { return this.UIDocument1MicrosoftWoWindow.UIItemWindow.UILinksToolBar.UIHyperlinkButton; } }
        public WinToolBar uILinksToolBar { get { return this.UIDocument1MicrosoftWoWindow.UIItemWindow.UILinksToolBar; } }
   
        //Click on ribbonButton
        public void ClickRibbonButton(string tabName)
        {
            WinTabPage uIInsertTab = new WinTabPage(uIRibbonTab);
            uIInsertTab.SearchProperties[WinTabPage.PropertyNames.Name] = tabName; 
           
            uIInsertTab.FindMatchingControls();
            Mouse.Click(uIInsertTab);
        }

        //click on new hypernate
        public void CreateNewHybernate()
        {
            uILinksToolBar.FindMatchingControls();
            uILinksToolBar.DrawHighlight();
       
            uIHyperlinkButton.FindMatchingControls();
            uIHyperlinkButton.DrawHighlight();
            Mouse.Click(uIHyperlinkButton);
        }

        //Click File on Ribbon panel
        public void ClickFile()
        {
            WinButton uIFileTabButton = this.UIDocument1MicrosoftWoWindow.UIItemWindow.UIRibbonPropertyPage.UIFileTabButton;
            uIFileTabButton.FindMatchingControls();
            Mouse.Click(uIFileTabButton);
            Thread.Sleep(WAIT_TIME);
        }
    }
}

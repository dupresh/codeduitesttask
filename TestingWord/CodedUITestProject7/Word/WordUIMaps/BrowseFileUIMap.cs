﻿namespace CodedUITestProject7.BrowseFileUIMapClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using System.Threading;


    public partial class BrowseFileUIMap
    {
        //time for waiting
        public const int WAIT_TIME = 2000;

        //save file
        public void SaveFileInDesktop(string fileName)
        {
            WinTreeItem uIDesktopTreeItem = this.UISaveAsWindow.UITreeViewWindow.UITreeViewTree.UIFavoritesTreeItem.UIDesktopTreeItem;
            WinButton uISaveButton = this.UISaveAsWindow.UISaveWindow.UISaveButton;
            WinComboBox uIFilenameComboBox = this.UISaveAsWindow.UIDetailsPanePane.UIFilenameComboBox;
            WinControl uIMicrosoftWordDialog = this.UIMicrosoftWordWindow.UIMicrosoftWordDialog;
            WinButton uIOKButton = this.UIMicrosoftWordWindow.UIMicrosoftWordDialog.UIOKButton;

            Mouse.Click(uIDesktopTreeItem);
            uIFilenameComboBox.EditableItem = fileName;
            Mouse.Click(uISaveButton);
            if (uIOKButton.Exists)
            {
                uIMicrosoftWordDialog.SetFocus();
                Thread.Sleep(WAIT_TIME);
                Mouse.Click(uIOKButton);
            }
        }
    }
}

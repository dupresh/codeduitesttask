﻿namespace CodedUITestProject7.InsertHyberlinkUIMapClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using System.Threading;


    public partial class InsertHyberlinkUIMap
    {
        //add link and appropriate for it 
        public void Hyberlink(String text, String link)
        {
            WinEdit textField = this.UIInsertHyperlinkWindow.UIItemWindow.UIItemEdit;
            WinEdit linkField = this.UIInsertHyperlinkWindow.UIItemWindow1.UIItemEdit;
            WinButton uIOKButton = this.UIInsertHyperlinkWindow.UIInsertHyperlinkDialog.UIOKButton;
            WinEdit uIAddressEdit = this.UIInsertHyperlinkWindow.UIInsertHyperlinkDialog.UIAddressEdit;
            WinListItem uIHttpgooglecomListItem = this.UIItemWindow.UIItemList.UIHttpgooglecomListItem;

            Mouse.Click(uIAddressEdit);
           
            linkField.Text = link;
            textField.Text = text;

            Mouse.Click(uIOKButton);
        }

       

    
}
}

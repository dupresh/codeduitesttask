﻿namespace CodedUITestProject7.WordUIMaps.SaveUIMapClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    
    
    public partial class SaveUIMap
    {
        //
        public void ClickSaveAs()
        {
            WinMenuItem uILocalSaveAsMenuItem = this.UIDocument1MicrosoftWoWindow.UIFileMenuBar.UILocalSaveAsMenuItem;
              
            uILocalSaveAsMenuItem.FindMatchingControls();
            uILocalSaveAsMenuItem.DrawHighlight();
            Mouse.Click(uILocalSaveAsMenuItem);
        }
    }
}

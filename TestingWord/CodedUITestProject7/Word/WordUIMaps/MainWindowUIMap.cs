﻿namespace CodedUITestProject7.WordUIMaps.MainWindowUIMapClasses
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using System.Threading;

    public partial class MainWindowUIMap
    {
        public const int WAIT_TIME = 2000;
       
        //close word
        public void CloseWord()
        {
            WinButton uICloseButton = this.UIDocument1MicrosoftWoWindow1.UIItemWindow.UIRibbonPropertyPage.UICloseButton;
            WinButton uINOButton = this.UIWorkSiteWindow.UINOWindow.UINOButton;
            WinControl uIWorkSiteDialog = this.UIWorkSiteWindow.UIWorkSiteDialog;
            
            uICloseButton.FindMatchingControls(); 
        }

        //choose NotSave
        public void ChooseNotSave()
        {
            WinButton uINOButton = this.UIWorkSiteWindow.UINOWindow.UINOButton;
            WinControl uIWorkSiteDialog = this.UIWorkSiteWindow.UIWorkSiteDialog;

            if (uIWorkSiteDialog.Exists)
            {
                Mouse.Click(uINOButton);
            }
            Thread.Sleep(WAIT_TIME);
        }
    }
}

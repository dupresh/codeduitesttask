﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by coded UI test builder.
//      Version: 11.0.0.0
//
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------

namespace CodedUITestProject7.WordUIMaps.DesctopUIMapClasses
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    
    
    [GeneratedCode("Coded UITest Builder", "11.0.60315.1")]
    public partial class DesctopUIMap
    {
        
        #region Properties
        public UIItemWindow UIItemWindow
        {
            get
            {
                if ((this.mUIItemWindow == null))
                {
                    this.mUIItemWindow = new UIItemWindow();
                }
                return this.mUIItemWindow;
            }
        }
        
        public UIProgramManagerWindow UIProgramManagerWindow
        {
            get
            {
                if ((this.mUIProgramManagerWindow == null))
                {
                    this.mUIProgramManagerWindow = new UIProgramManagerWindow();
                }
                return this.mUIProgramManagerWindow;
            }
        }
        #endregion
        
        #region Fields
        private UIItemWindow mUIItemWindow;
        
        private UIProgramManagerWindow mUIProgramManagerWindow;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "11.0.60315.1")]
    public class UIItemWindow : WinWindow
    {
        
        public UIItemWindow()
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.AccessibleName] = "Context";
            this.SearchProperties[WinWindow.PropertyNames.ClassName] = "#32768";
            #endregion
        }
        
        #region Properties
        public UIContextMenu UIContextMenu
        {
            get
            {
                if ((this.mUIContextMenu == null))
                {
                    this.mUIContextMenu = new UIContextMenu(this);
                }
                return this.mUIContextMenu;
            }
        }
        #endregion
        
        #region Fields
        private UIContextMenu mUIContextMenu;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "11.0.60315.1")]
    public class UIContextMenu : WinMenu
    {
        
        public UIContextMenu(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinMenu.PropertyNames.Name] = "Context";
            #endregion
        }
        
        #region Properties
        public WinMenuItem UIOpenMenuItem
        {
            get
            {
                if ((this.mUIOpenMenuItem == null))
                {
                    this.mUIOpenMenuItem = new WinMenuItem(this);
                    #region Search Criteria
                    this.mUIOpenMenuItem.SearchProperties[WinMenuItem.PropertyNames.Name] = "Open";
                    #endregion
                }
                return this.mUIOpenMenuItem;
            }
        }
        #endregion
        
        #region Fields
        private WinMenuItem mUIOpenMenuItem;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "11.0.60315.1")]
    public class UIProgramManagerWindow : WinWindow
    {
        
        public UIProgramManagerWindow()
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.Name] = "Program Manager";
            this.SearchProperties[WinWindow.PropertyNames.ClassName] = "Progman";
            this.WindowTitles.Add("Program Manager");
            #endregion
        }
        
        #region Properties
        public UIDesktopList UIDesktopList
        {
            get
            {
                if ((this.mUIDesktopList == null))
                {
                    this.mUIDesktopList = new UIDesktopList(this);
                }
                return this.mUIDesktopList;
            }
        }
        
        public UIFolderViewWindow UIFolderViewWindow
        {
            get
            {
                if ((this.mUIFolderViewWindow == null))
                {
                    this.mUIFolderViewWindow = new UIFolderViewWindow(this);
                }
                return this.mUIFolderViewWindow;
            }
        }
        #endregion
        
        #region Fields
        private UIDesktopList mUIDesktopList;
        
        private UIFolderViewWindow mUIFolderViewWindow;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "11.0.60315.1")]
    public class UIDesktopList : WinList
    {
        
        public UIDesktopList(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinList.PropertyNames.Name] = "Desktop";
            this.WindowTitles.Add("Program Manager");
            #endregion
        }
        
        #region Properties
        public WinListItem UINewMicrosoftWordDocuListItem
        {
            get
            {
                if ((this.mUINewMicrosoftWordDocuListItem == null))
                {
                    this.mUINewMicrosoftWordDocuListItem = new WinListItem(this);
                    #region Search Criteria
                    this.mUINewMicrosoftWordDocuListItem.SearchProperties[WinListItem.PropertyNames.Name] = "New Microsoft Word Document";
                    this.mUINewMicrosoftWordDocuListItem.WindowTitles.Add("Program Manager");
                    #endregion
                }
                return this.mUINewMicrosoftWordDocuListItem;
            }
        }
        
        public WinListItem UITestFileListItem
        {
            get
            {
                if ((this.mUITestFileListItem == null))
                {
                    this.mUITestFileListItem = new WinListItem(this);
                    #region Search Criteria
                    this.mUITestFileListItem.SearchProperties[WinListItem.PropertyNames.Name] = "testFile";
                    this.mUITestFileListItem.WindowTitles.Add("Program Manager");
                    #endregion
                }
                return this.mUITestFileListItem;
            }
        }
        
        public WinListItem UITestFileListItem1
        {
            get
            {
                if ((this.mUITestFileListItem1 == null))
                {
                    this.mUITestFileListItem1 = new WinListItem(this);
                    #region Search Criteria
                    this.mUITestFileListItem1.SearchProperties[WinListItem.PropertyNames.Name] = "testFile";
                    this.mUITestFileListItem1.WindowTitles.Add("Program Manager");
                    #endregion
                }
                return this.mUITestFileListItem1;
            }
        }
        #endregion
        
        #region Fields
        private WinListItem mUINewMicrosoftWordDocuListItem;
        
        private WinListItem mUITestFileListItem;
        
        private WinListItem mUITestFileListItem1;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "11.0.60315.1")]
    public class UIFolderViewWindow : WinWindow
    {
        
        public UIFolderViewWindow(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.ControlId] = "1";
            this.WindowTitles.Add("Program Manager");
            #endregion
        }
        
        #region Properties
        public WinList UIDesktopList
        {
            get
            {
                if ((this.mUIDesktopList == null))
                {
                    this.mUIDesktopList = new WinList(this);
                    #region Search Criteria
                    this.mUIDesktopList.SearchProperties[WinList.PropertyNames.Name] = "Desktop";
                    this.mUIDesktopList.WindowTitles.Add("Program Manager");
                    #endregion
                }
                return this.mUIDesktopList;
            }
        }
        #endregion
        
        #region Fields
        private WinList mUIDesktopList;
        #endregion
    }
}

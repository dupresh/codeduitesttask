﻿namespace CodedUITestProject7.StartDocUIMapClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    
    
    public partial class StartDocUIMap
    {
        public static int WAIT_TIME = 2000;

        public void NewDoc()
        {
            WinListItem uIBlankdocumentListItem = this.UIDocument2WordWindow.UIFeaturedList.UIBlankdocumentListItem;
            uIBlankdocumentListItem.WaitForControlExist(WAIT_TIME);

            if (uIBlankdocumentListItem.Exists)
            {
                Mouse.Click(uIBlankdocumentListItem);
            }
        }
    }
}

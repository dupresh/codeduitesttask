﻿namespace CodedUITestProject7.WordUIMaps.DesctopUIMapClasses
{
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;


    public partial class DesctopUIMap
    {
        //find needfull file on desktop
        public void ChooseDocFile(String fileName)
        {
            WinMenuItem uIOpenMenuItem = this.UIItemWindow.UIContextMenu.UIOpenMenuItem;
            WinList UIDesktopList = this.UIProgramManagerWindow.UIDesktopList;
            
            UIDesktopList.SetFocus();
            UIDesktopList.DrawHighlight();
            UITestControlCollection collectionOfDocs = UIDesktopList.GetChildren();
            foreach (UITestControl docsControl in collectionOfDocs)
            {
                if (docsControl.Name.Equals(fileName))
                {
                    Mouse.Click(docsControl, MouseButtons.Right);
                    Mouse.Click(uIOpenMenuItem);
                }
            }
        }
    }
}

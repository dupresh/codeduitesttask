﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodedUITestProject7.TestData;

namespace CodedUITestProject7
{

    [CodedUITest]
    public class CodedUITest1
    {
        //class is starting procces of Microsoft Word
        [ClassInitialize()]
        public static void ClassInit(TestContext TestContext)
        {
            Bind.appUtil.KillAllAppProcess(TestPropData.NAME_OF_WORD_PROCESS);
            Bind.appUtil.StartApplication(TestPropData.PATH_OF_WORD);
        }

        [TestInitialize()]
        public void TestInit()
        {
        }

        //Create a hyperlink getting data from .xls
        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\TestData\\Test.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "1$", DataAccessMethod.Sequential)]
        [TestMethod]
        public void CodedUITestCreateHyperlink()
        {
            Bind.ribbonMap.ClickRibbonButton(TestPropData.INSERT);
            Bind.ribbonMap.CreateNewHybernate();
            Bind.hyberlinkMap.Hyberlink(TestContext.DataRow["text"].ToString(), TestContext.DataRow["link"].ToString());
            Keyboard.SendKeys("{Enter}");
        }

        //click home on ribbon panel
        [TestCleanup()]
        public void TestCleaning()
        {
            Bind.ribbonMap.ClickRibbonButton(TestPropData.HOME);
        }

        [ClassCleanup()]
        public static void MYClassCleanup()
        {
        }

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

    }
}



﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Diagnostics;
using CodedUITestProject7.TestData;

namespace CodedUITestProject7
{
    
    [CodedUITest]
    public class CodedUITest2
    {
        //get opened process from CodedUITestCreateHyperlink
        [ClassInitialize()]
        public static void ClassInit(TestContext testContext)
        {
            Bind.appUtil.ShowTheDesktop();
            Bind.appUtil.GetProcessByName(TestPropData.NAME_OF_WORD_PROCESS);          
        }

        //save file 
        [TestInitialize()]
        public void TestInit()
        {
            Bind.ribbonMap.ClickFile();
            Bind.saveMap.ClickSaveAs();
            Bind.filetreeMap.SaveFileInDesktop(TestPropData.NAME_OF_FILE);
            Bind.mainWindMap.CloseWord();
        }

        //open file on desktop 
        [TestMethod]
        public void CodedUITestCheckSave()
        {
            Bind.desktopMap.ChooseDocFile(TestPropData.NAME_OF_FILE);   
        }

        //showing opened window for 5 sec and close it
        [TestCleanup()]
        public void TestCleanUp()
        {
            Thread.Sleep(5000);
            Bind.appUtil.CloseClient();
            Bind.mainWindMap.CloseWord();
            Bind.mainWindMap.ChooseNotSave();
        }

        [ClassCleanup()]
        public static void ClassCleanUp()
        {
        }

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}


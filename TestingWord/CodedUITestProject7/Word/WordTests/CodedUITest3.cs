﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodedUITestProject7.TestData;
using System.Threading;
using System.Windows.Forms;


namespace CodedUITestProject7.WordTests
{

    [CodedUITest]
    public class CodedUITest3
    {
        //Open Word but before close all word procceses
        [ClassInitialize()]
        public static void ClassInit(TestContext TestContext)
        {
            Bind.appUtil.KillAllAppProcess(TestPropData.NAME_OF_WORD_PROCESS);
            Bind.appUtil.StartApplication(TestPropData.PATH_OF_WORD);
            Thread.Sleep(2000);
        }

        [TestInitialize()]
        public void TestInit(){}

        //Entering some text and after check that it is exist
        [TestMethod]
        public void EnterSomeText()
        {
            Bind.workWord.AddText(TestPropData.TEXT_ONE);
            Bind.workWord.AddText(TestPropData.TEXT_TWO);
            string compareWithIt = TestPropData.TEXT_ONE + TestPropData.CONTROL_R + TestPropData.TEXT_TWO + TestPropData.CONTROL_R;
            Assert.AreEqual(compareWithIt, Bind.workWord.GetAllText());
        }

        //Close Word
        [TestCleanup()]
        public void TestCleaning()
        {
            Bind.appUtil.CloseClient();
            Bind.mainWindMap.CloseWord();
            Bind.mainWindMap.ChooseNotSave();
        }

        //Kill all Word procces
        [ClassCleanup()]
        public static void ClassCleainig()
        {    
            Bind.appUtil.KillAllAppProcess(TestPropData.NAME_OF_WORD_PROCESS);
        }

    
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CodedUITestProject7.StartDocUIMapClasses;
using CodedUITestProject7.WordUIMaps.RibbonPanelUIMapClasses;
using CodedUITestProject7.InsertHyberlinkUIMapClasses;
using CodedUITestProject7.WordUIMaps.MainWindowUIMapClasses;
using CodedUITestProject7.WordUIMaps.SaveUIMapClasses;
using CodedUITestProject7.BrowseFileUIMapClasses;
using CodedUITestProject7.WordUIMaps.DesctopUIMapClasses;

using CodedUITestProject7.Excel.ExcelUI.MainElementUIMapClasses;

using CodedUITestProject7.Utils;

using CodedUITestProject7.WordApi;

using CodedUITestProject7.ExcelAut.ExcelAppi;





namespace CodedUITestProject7
{
    public class Bind
    {
        ///Word
        //UIMaps of Word
        public static RibbonPanelUIMap ribbonMap = new RibbonPanelUIMap();
        public static MainWindowUIMap mainWindMap = new MainWindowUIMap();
        public static StartDocUIMap startMap = new StartDocUIMap();
        public static InsertHyberlinkUIMap hyberlinkMap = new InsertHyberlinkUIMap();
        public static SaveUIMap saveMap = new SaveUIMap();
        public static BrowseFileUIMap filetreeMap = new BrowseFileUIMap();
        public static DesctopUIMap desktopMap = new DesctopUIMap();
        //Docs application
        public static WorkWithWord workWord = new WorkWithWord();
        
        ///Excel
        //UIMaps of Excel
        public static MainElementUIMap mainElemMap = new MainElementUIMap();
        //Excel application
        public static WorkWithExcel workExcel = new WorkWithExcel();

        ///General
        //Utils
        public static ApplicationUtils appUtil = new ApplicationUtils();

        
    }
}

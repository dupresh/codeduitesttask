﻿namespace CodedUITestProject7.Excel.ExcelUI.MainElementUIMapClasses
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Input;
    using System.CodeDom.Compiler;
    using System.Text.RegularExpressions;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    
    
    public partial class MainElementUIMap
    {
        public void CloseExcel()
        {
            WinButton uICloseButton = this.UIMicrosoftExcelBook2Window.UIItemWindow.UIRibbonPropertyPage.UICloseButton;
            Mouse.Click(uICloseButton);
        }

        public void NotSaveDocument()
        {
            WinButton uINOButton = this.UIWorkSiteWindow.UINOWindow.UINOButton;
            if (uINOButton.Exists)
            {
                Mouse.Click(uINOButton);
            }
        }
    }
}

﻿namespace CodedUITestProject7.ExcelAut.ExcelAppi
{

using Microsoft.Office.Interop.Excel;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Collections;

    public class WorkWithExcel
    {
        private static Excel.Application excelApp;
        private static Excel.Sheets excelSheets;
        private static Excel.Worksheet excelWorksheet;
        private static Excel.Range excelRange;

        public void EnterTextInFirstCell(string text)
        {
            excelApp = (Application)Marshal.GetActiveObject("Excel.Application");
            excelApp.Visible = true;
            excelWorksheet = excelApp.Worksheets.Application.ActiveSheet;
            excelSheets = excelApp.Worksheets;
            excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
            excelRange = excelWorksheet.get_Range("A1", Type.Missing);
            
            Keyboard.SendKeys(text);
            Keyboard.SendKeys("{Enter}");
        }

        public void EnterTextInNextCell(string text)
        {
            excelApp = (Application)Marshal.GetActiveObject("Excel.Application");
            excelApp.Visible = true;
            excelWorksheet = excelApp.Worksheets.Application.ActiveSheet;
            excelSheets = excelApp.Worksheets;
            excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
            excelRange = excelWorksheet.get_Range("R1", Type.Missing);
            String Cell = excelRange.get_Value(); 
            while (!String.IsNullOrEmpty(Cell)){}          
            Keyboard.SendKeys(text);
            Keyboard.SendKeys("{Enter}");
        }

        public static int GetRowsCount()
        {
            excelApp = (Application)Marshal.GetActiveObject("Excel.Application");
            excelApp.Visible = true;
            Thread.Sleep(3000);

            excelWorksheet = excelApp.Worksheets.Application.ActiveSheet;

            return excelWorksheet.UsedRange.Rows.Count;
        }

        public string GetTextFromCell()
        {
            excelApp = (Application)Marshal.GetActiveObject("Excel.Application");
            excelApp.Visible = true;
            excelWorksheet = excelApp.Worksheets.Application.ActiveSheet;
            excelSheets = excelApp.Worksheets;
            excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(1);
            excelRange = excelWorksheet.get_Range("A1".ToString(), Type.Missing);
            string text = "";
            Range row = excelWorksheet.UsedRange.Rows;
            foreach (Range cell in row.Cells)
            {
                text += cell.Value.ToString();
            }
            return text;
        }

        public bool IsTextPresent(ArrayList text, string fullText)
        {
            foreach (string arString in text)
            {
                if (fullText.Contains(arString))
                    return true;
            }
            return false;
        }
    }
}
 
﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.Threading;
using CodedUITestProject7.TestData;
using System.Collections;


namespace CodedUITestProject7.ExcelTest
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class CodedUITest4
    {
        [ClassInitialize()]
        public static void ClassInit(TestContext TestContext)
        {
            Bind.appUtil.KillAllAppProcess(TestPropData.NAME_OF_EXCEL_PROCESS);
            Bind.appUtil.StartApplication(TestPropData.PATH_OF_EXCEL);
            Thread.Sleep(2000);
        }

        [TestInitialize()]
        public void TestInit()
        {
            Thread.Sleep(2000);
        }

        [TestMethod]
        public void EnterTextInExcel()

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        




































        
        
        
        
        
        
        
        
        
        
        {
            Bind.workExcel.EnterTextInFirstCell(TestPropData.TEXT_ONE);
            Bind.workExcel.EnterTextInNextCell(TestPropData.TEXT_TWO);
            ArrayList listOfText = new ArrayList();
            listOfText.Add(TestPropData.TEXT_ONE);
            listOfText.Add(TestPropData.TEXT_TWO);
            string text = Bind.workExcel.GetTextFromCell();
            Assert.IsTrue(Bind.workExcel.IsTextPresent(listOfText, text));
        }

        [TestCleanup()]
        public void TestCleaning()
        {
            Bind.appUtil.CloseClient();
            Bind.mainElemMap.NotSaveDocument();
            Bind.mainElemMap.CloseExcel();
        }

        [ClassCleanup()]
        public static void ClassCleainig()
        {
            
            Bind.appUtil.KillAllAppProcess(TestPropData.NAME_OF_WORD_PROCESS);
        }
            
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
